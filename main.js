/**
 * Created by stephen on 14.02.17.
 */

function cutSpaces(str) {
    return str.replace(/(\s+)(?=\S)/g, "");
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function horyline() {
    $(".log-block").prepend('<hr color="#000">');
}

function log(msg, cls) {
    $(".log-block").prepend('<p class="' + cls + '">' + msg + "</p>");
}

function debugInfo(found, pattern) {
    var msg = "Нашел \'" + found + "\'.  ";
    msg += "Следующий поиск начнется с " + pattern.lastIndex;
    log(msg, "ok");
}

function incValue(rowId) {
    var selector = $(rowId + " .value");
    selector.html(parseInt(selector.html()) + 1);
}

function displayErrPos(pos) {
    inputValue = $("#expr").val();
    var len = inputValue.length;
    var str = "";

    if (pos == len) {
        ++len;
        $("#expr").val(inputValue + ' '); //Жесткий костыль
    }

    for (var i = 0; i < len; ++i) {
        if (i != pos)
            str += ' ';
        else
            str += '^';
    }

    $(".errpos").html(str);
    $("#expr").css("border-color", "red");
    $(".errpos").show();
}

function unknownLex(pos) {
    log("Позиция " + pos + ": неизвестная лексема", "err");
    displayErrPos(pos);
}

$(document).ready(function () {
    $("#btn").click(function () {
        if ($("#expr").val() == "") {
            alert("Введите математическое выражение в поле ввода");
            return;
        }

        horyline();

        var str = $("#expr").val();

        $(".token-count .value").html(0);

        var states = {
            EXPR: 0,
            BIN: 1,
            OP: 2,
            PAR: 3,
            ERR: 4
        };

        var signs = {
            DIG: 2,
            FUNC: 5,
            BIN: 6,
            UN: 7,
            OPPAR: 8,
            CLPAR: 9
        };

        var table_sign = [
            null, null, 0, null, null, 1, 2, 3, 4, 5, 0
        ];

        var trek_table = [
            [1, 3, 4, 2, 0, 1],
            [4, 4, 2, 2, 4, 1],
            [1, 3, 4, 4, 0, 4],
            [4, 4, 4, 4, 0, 4]
        ];

        function expected(state) {
            if (state == states.EXPR || state == states.OP) {
                return "ожидалось выражение";
            } else if (state == states.BIN) {
                return "ожидалась операция";
            } else if (state == states.PAR) {
                return "ожидалась открывающая скобка";
            }
        }

        function get(sign) {
            switch (sign) {
                case signs.DIG:
                    return "получено число";
                case signs.FUNC:
                    return "получена функция";
                case signs.BIN:
                    return "получена операция";
                case signs.UN:
                    return "получена операция";
                case signs.OPPAR:
                    return "получена открывающая скобка";
                case signs.CLPAR:
                    return "получена закрывающая скобка";
                default:
                    break;
            }
        }

        function genError(pos, state, sign) {
            var msg = "Позиция " + pos + ": " + expected(state);
            if (sign != undefined) 
                msg += ", " + get(sign);
            return msg;
        }

        var curState = states.EXPR;
        var prevState = curState;
        var pattern = /\s*((\d+([.]?(\d*[Ee][+-]?)?\d+)?)|(sin|cos|exp|sqrt|log|abs|max|min)|([+*\/^])|([-])|([(])|([)])|([a-zA-Z]))/g;
        var match;
        var lastIndex = 0;
        var lexCount = {
            '+'      :0,
            '-'      :0,
            '*'      :0,
            '/'      :0,
            '^'      :0,
            'sin'    :0,
            'cos'    :0,
            'exp'    :0,
            'log'    :0,
            'sqrt'   :0,
            'abs'    :0,
            'min'    :0,
            'max'    :0,
            'const'  :0,
            'x'      :0,
            '('      :0,
            ')'      :0
        };

        while ((match = pattern.exec(str)) != null) {
            if (match.index != lastIndex) {
                unknownLex(lastIndex);
                prevState = curState;
                curState = states.ERR;
                break;
            }

            var key = cutSpaces(match[0]);
            if (isNumeric(key))
                key = "const";

            if (lexCount[key] !== undefined)
                lexCount[key] += 1;
            else {
                unknownLex(lastIndex);
                prevState = curState;
                curState = states.ERR;
                return;
            }

            debugInfo(match[0], pattern);

            var sign;

            for (var i = 2; i <= 10; ++i) {
                if (match[i] != undefined) {
                    sign = i;
                    prevState = curState;
                    curState = trek_table[curState][table_sign[sign]];
                    break;
                }
            }

            if (curState == states.ERR) {
                log(genError(lastIndex, prevState, sign), "err");
                displayErrPos(lastIndex);
                break;
            }

            lastIndex = pattern.lastIndex;
        }

        if (curState == states.EXPR || curState == states.OP) {
            log(genError(lastIndex, curState), "err");
            displayErrPos(lastIndex);
        }
        if (curState == states.BIN)
            log("Выражение успешно разобрано", "ok");

        $(".token-count .value").each(function () {
            $(this).html(lexCount[$(this).attr("ids")]);
        });
    });

    $("#expr").focus(function () {
        $(".errpos").hide();
        $("#expr").css("border-color", "#999");
    });
});